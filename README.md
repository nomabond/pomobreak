# Pomodoro Break

Pomodoro Breaker is a simple Python application to help you manage break and focus intervals for intense focus sessions.


The program is fairly straight forward to use. If you have [Python 3.X](https://www.python.org/downloads/) installed you can just run the pomodoro.pyw file. 
 
If you have no Python installed and don't have any intentions to install Python you can use the [Windows Installer](https://bitbucket.org/nomabond/pomobreak/src/0cd9aa5b93fa/installer/?at=master)


#### To use the program:

 - Set the time you will stop working using the time pickers and check the "Choose for me" box
 - ![alt text](https://dl.dropboxusercontent.com/u/122986229/pomoreadmeimages/timepicker.JPG "Time Picker")
![alt text](https://dl.dropboxusercontent.com/u/122986229/pomoreadmeimages/choose.JPG "Choose For Me")
## OR
 - Enter your own Focus and Break time intervals and click Start Clock

 - ![alt text](https://dl.dropboxusercontent.com/u/122986229/pomoreadmeimages/enabled.JPG "Focus Mode Go!")


#### A subtle alarm tone will notify you of the end of break time.


### Version
1.0.0


### Todos

 - Write Tests
 - Add Code Comments
 - Add Night Mode

License
----

MIT

### Contact
If you find anything wrong with this project or want to contribute [FORK IT](https://bitbucket.org/nomabond/pomobreak/fork) and submit a [PR](https://bitbucket.org/nomabond/pomobreak/pull-requests/new) or [contact me](mailto:austinaryain@gmail.com)