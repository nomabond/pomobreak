import os
import sys
import time
import random
import datetime
import winsound
import threading
import webbrowser
import Tkinter as Tk


class App(object):

    def __init__(self):
        # Constant for conversion to seconds #set to 10 for testing.
        self.MINUTES_TO_SECONDS = 60  # 1
        # Min and max for Reasonable Pomodoro Method
        self.MIN_FOCUS = 25
        self.MIN_BREAK = 5
        self.MAX_FOCUS = 50
        self.MAX_BREAK = 25
        self.isChecked = False

        self.distractionList = ["https://www.youtube.com/feed/trending", "https://www.reddit.com/rising/", "https://www.reddit.com/new/",
                                "http://reddit.com/top/", "http://www.kongregate.com/", "https://medium.com/browse/top", "https://www.buzzfeed.com/"]

        self.total_breaks = 3
        self.break_count = 0
        # setup the root
        self.clockthread = threading.Thread(
            target=self.startclock)
        self.clockthread.daemon = True

        self.root = Tk.Tk()
        self.root.resizable(width=False, height=False)
        self.root.wm_title("Pomo Breaker")

        # WINDOW GEOMETRY
        w = 300  # width
        h = 450  # height
        sw = self.root.winfo_screenwidth()  # screenwidth
        sh = self.root.winfo_screenheight()  # screenheight
        x = (sw / 2) - (w / 2)  # X Pos
        y = (sh / 2) - (h / 2)  # Y Pos
        self.root.geometry('%dx%d+%d+%d' % (w, h, x, y))

        self.root.columnconfigure(0, weight=1)

        # Add widgets

        self.timestoplabel = Tk.Label(
            self.root, text="Enter the time you will stop working: ")
        self.timestoplabel.pack(fill=Tk.X, pady=2)

        # Top frame for horizontal widgets
        self.topframe = Tk.Frame(self.root)
        self.topframe.pack(padx=64, fill=Tk.X, side=Tk.TOP)

        # StringVars for default time picker options
        self.hourvariable = Tk.StringVar(self.root)
        self.minutevariable = Tk.StringVar(self.root)
        self.ampmvariable = Tk.StringVar(self.root)
        self.hourvariable.set("12")
        self.minutevariable.set("00")
        self.ampmvariable.set("PM")

        # Setup and pack the finish time picker.
        self.houroption = Tk.OptionMenu(
            self.root, self.hourvariable, "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12")
        self.minuteoption = Tk.OptionMenu(
            self.root, self.minutevariable, "00", "15", "20", "30", "40", "45")
        self.ampmoption = Tk.OptionMenu(
            self.root, self.ampmvariable, "PM", "AM")
        self.houroption.pack(in_=self.topframe, side=Tk.LEFT)
        self.ampmoption.pack(in_=self.topframe, side=Tk.RIGHT)
        self.minuteoption.pack(in_=self.topframe, side=Tk.RIGHT)

        self.checkboxstatus = Tk.IntVar()
        self.autopick = Tk.Checkbutton(self.root, text="Choose for me", variable=self.checkboxstatus,
                                       command=self.onchecked, font=("Helvetica", 12, "bold"))
        self.autopick.pack(pady=8)

        self.focuslabel = Tk.Label(self.root,
                                   text="Enter the time to focus(minutes)[25-50]: ")
        self.focuslabel.pack(fill=Tk.X, pady=2)

        self.focus_minutes = Tk.StringVar()
        self.focusentry = Tk.Entry(self.root, text=self.focus_minutes)
        self.focusentry.pack(fill=Tk.X, pady=5, padx=20)
        self.focusentry.focus()

        self.breaklabel = Tk.Label(self.root,
                                   text="Enter the time to break(minutes)[5-25]: ")
        self.breaklabel.pack(fill=Tk.X, pady=2)
        self.break_minutes = Tk.StringVar()
        self.breakentry = Tk.Entry(self.root, text=self.break_minutes)
        self.breakentry.pack(fill=Tk.X, pady=5, padx=20)

        self.startbuttontext = Tk.StringVar()
        self.startbuttontext.set("Start Clock")
        self.start_button = Tk.Button(self.root, textvariable=self.startbuttontext,
                  command=self.clickstart, font=("Helvetica", 12, "bold")).pack(fill=Tk.X, padx=20, ipady=10)

        self.resetbuttontext = Tk.StringVar()
        self.resetbuttontext.set("Reset")
        Tk.Button(self.root, textvariable=self.resetbuttontext,
                  command=self.clickreset, font=("Helvetica", 12, "bold")).pack(fill=Tk.X, padx=20, ipady=10)

        self.infolabel = Tk.Label(
            self.root, text="", wraplength=250, justify=Tk.CENTER)
        self.infolabel.pack(fill=Tk.X, pady=20)

        self.quitbuttontext = Tk.StringVar()
        self.quitbuttontext.set("Quit")
        Tk.Button(self.root, textvariable=self.quitbuttontext,
                  command=self.clickstop).pack(fill=Tk.X, side=Tk.BOTTOM, padx=40, pady=2)

        # Start this badboy up!
        self.root.mainloop()

    def onchecked(self):
        if self.isChecked:
            self.focusentry.configure(state="normal")
            self.breakentry.configure(state="normal")
            self.focusentry.delete(0, Tk.END)
            self.breakentry.delete(0, Tk.END)
            self.isChecked = False
        else:
            generated_times = self.generate_times()
            self.focus_minutes.set(generated_times[0])
            self.break_minutes.set(generated_times[1])
            self.infolabel.config(
                text="You can smash out {0} focus sessions at this pace! Go get it!".format(self.total_breaks))
            self.focusentry.configure(state="disabled")
            self.breakentry.configure(state="disabled")
            self.isChecked = True

    def generate_times(self):
        list_focus = [30, 40, 45, 50]
        list_break = [5, 10, 15, 20]

        focusamount = list_focus[random.randrange(0, 4)]
        breakamount = list_break[random.randrange(0, 4)]

        current_time = str(datetime.datetime.now()).split(" ")[1][:5]
        if self.ampmvariable.get() == "PM" and int(self.hourvariable.get()) < 12:
            finish_time = str(int(self.hourvariable.get()) +
                              12) + ":" + self.minutevariable.get()
        elif int(self.hourvariable.get()) == 12 and self.ampmvariable.get() == "AM":
            finish_time = "00:" + self.minutevariable.get()
        else:
            finish_time = self.hourvariable.get() + ":" + self.minutevariable.get()

        FMT = "%H:%M"
        tdelta = str(datetime.datetime.strptime(finish_time, FMT) -
                     datetime.datetime.strptime(current_time, FMT)).split(" ")

        if len(tdelta) > 1:
            tdelta = tdelta[2][:5]
        else:
            tdelta = tdelta[0][:5]

        if tdelta.endswith(":"):
            tdelta = "0" + tdelta[:4]

        numbreaks = (
            ((int(tdelta[:2]) * 60) + (int(tdelta[3:])) / (focusamount + breakamount)) // 60)

        self.total_breaks = int(numbreaks)

        print(str(numbreaks))
        print(current_time + " : " + finish_time + " : " + tdelta)
        print("Focus time: " + str(focusamount) +
              " Break time: " + str(breakamount))

        return [focusamount, breakamount]

    def startclock(self):
        # Ensure focus and break times are within reasonable bounds for
        # Pomodoro Method.

        if self.focus_minutes <= self.MIN_FOCUS:
            self.focus_minutes = self.MIN_FOCUS
        elif self.focus_minutes >= self.MAX_FOCUS:
            self.focus_minutes = self.MIN_FOCUS

        if self.break_minutes <= self.MIN_BREAK:
            self.break_minutes = self.MIN_BREAK
        elif self.break_minutes >= self.MAX_BREAK:
            self.break_minutes = self.MAX_BREAK

        while self.break_count < self.total_breaks:
            self.infolabel.config(
                text="Focus mode enabled for {0} minutes. You can do it!".format(self.focus_minutes))
            time.sleep(self.focus_minutes * self.MINUTES_TO_SECONDS)
            # Open a web browser to some distracting stuff!
            webbrowser.open(self.distractionList[
                random.randrange(0, len(self.distractionList))])
            self.infolabel.config(
                text="Break mode enabled for {0} minutes. Relax! Recharge! And get ready for your next Focus!".format(self.break_minutes))
            time.sleep(self.break_minutes * self.MINUTES_TO_SECONDS)
            self.break_count = self.break_count + 1
            winsound.PlaySound(os.path.dirname(__file__) +
                               "/resources/bell.wav", winsound.SND_FILENAME)

    def clickstart(self):
        # create our clockthread to manage the clock data
        if self.clockthread.isAlive():
            return
        else:
            # Check for valid integer input
            try:
                self.break_minutes = int(self.break_minutes.get())
                self.focus_minutes = int(self.focus_minutes.get())
            except ValueError:
                self.infolabel.config(text="ERROR: Invalid number.")
                self.focus_minutes.set("")
                self.break_minutes.set("")
                return
        self.root.iconify()

        self.clockthread.start()

    def clickstop(self):
        self.clockthread.stopped = True
        self.root.destroy()

    def clickreset(self):
        python = sys.executable
        os.execl(python, python, * sys.argv)

    def button_click(self, e):
        pass

App()
